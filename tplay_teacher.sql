/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : studentadmin

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-06-02 19:50:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tplay_teacher
-- ----------------------------
DROP TABLE IF EXISTS `tplay_teacher`;
CREATE TABLE `tplay_teacher` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '职称',
  `class` varchar(255) DEFAULT NULL COMMENT '教学班级',
  `sex` tinyint(1) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `born` varchar(255) DEFAULT NULL COMMENT '出生日期',
  `Nation` varchar(255) DEFAULT NULL COMMENT '民族',
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `idcard` varchar(255) DEFAULT NULL COMMENT '身份证',
  `retire` tinyint(1) DEFAULT '0' COMMENT '是否退休',
  `Politics` varchar(255) DEFAULT NULL COMMENT '政治面貌',
  `Education` varchar(255) DEFAULT NULL COMMENT '学历',
  `subject` varchar(255) DEFAULT NULL COMMENT '所教学科',
  `school` varchar(255) DEFAULT NULL COMMENT '毕业学校',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
